import React from 'react';

export default ({data}) => {
    console.log(data, "cards")

    return  data.map((card,i) => {
        console.log(card.name, "card")

        return (
            <div key={String(Symbol(i))} className="single-card" id={card.id} > 
            <p className="name">{card.name}</p>
            </div>
        )
    })
}

