import React from 'react';
import { getCards, createCard } from '../Data/getDataFromTrello';
import Card from '../Cards/Cards';
import CardElement from "../Cards/CardElement";


class ListElement extends React.Component{
    constructor(props){
        super(props);
        this.state = {
                cardData: []
        }
    }

    getCardName = (name, id) => {
        // console.log(name, id, " entered get card")
        if(name && id) {
            createCard(name, id)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    cardData: [...this.state.cardData,  data]
                })
            },() => console.log(this))
            .catch(console.log)
        } else {
            return
        }
    }
    componentDidMount() {
        this.props.listData.forEach((element) => {
            // console.log(element, " entered get card")
            getCards(element.id)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    cardData: data
                })
            })
            .catch(console.log)
        });
    }
    render(){

        return (
            this.props.listData.map((data,i) => {
                console.log(data, " list data")
            return (
                <div className="list-div" key={String(Symbol(i))}  >
                    <div className="head-name">{data.name}</div>
                    <div className="cardContainer">
                    {this.state.cardData.length > 0 && <Card data={this.state.cardData.filter(el => el.idList === data.id)}/>}
                    <CardElement id={data.id} getName={this.getCardName}/>
                    </div>
                </div>
            )
        })
      )
        }

}

export default ListElement;