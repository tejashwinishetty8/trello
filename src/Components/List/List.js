import React from 'react';
import { getTrelloList, createTrelloList } from '../Data/getDataFromTrello';
import ListElement from './ListElement';
import ListInput from './ListInput';
import ListAddBtn from './ListAddBtn'

class List extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.id,
            showInput: false,
            inputName:"",
            ListData: [],
        }
        this.AddList = this.AddList.bind(this);
    }

    AddList(event){
        console.log(event.target.parentNode.previousSibling);
    }
    handleClose = () => {
        this.setState({
            showInput: false
        })
    }

    handleClick = () => {
        this.setState({
            showInput: true
        })
    }
    getName = (e) => {
        this.setState({
            inputName: e.target.value.trim() 
        })
    }

    getInputValue = (e) => {
        const listName = this.state.inputName;
        if(listName) {
            createTrelloList(listName)
            .then(response => response.json())
            .then(data => {
                console.log(data, " before")
                data["cards"] = [];
                this.setState({
                    ListData: [...this.state.ListData, data],
                    inputName:''
                })
            },() => {console.log(this.state.ListData, " after")})  
            .catch(console.log)
        }else {
            return
        }
    }
    componentDidMount() {
        getTrelloList().then(resp => resp.json())
            .then(data => {
                this.setState({
                    ListData: data
                } ,()=>{console.log()})
            });
    }
    
    render() {
        let arr = [...this.state.ListData];
        // console.log(arr, " arr");
        return ( <div className = "List" style = {{"display":"flex", "justifyContent":"space-around"}}> {this.state.ListData.length > 0 ? < ListElement listData = { arr }/> :
             <p className="loader">Loading existing cards...</p >} 
            {this.state.showInput ? <ListInput clickEvent={this.handleClose} getName={this.getName} val={this.state.inputName} getInputValue={this.getInputValue} /> : <ListAddBtn clickEvent={this.handleClick}/>}
        </div>
        )
    }
}

export default List;