import React from "react";

export default ({clickEvent}) => (
    <div><div style = {{"cursor":"pointer"}} className="btn-style" onClick={() => clickEvent()} >
        <span>+</span> Add another list
    </div></div>
)