import React, { Component } from 'react';
import './App.css';
import Board from './Components/BoardContainer'
import List from './Components/List/List'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Board />
        <List />
      </div>
    );
  }
}

export default App;
