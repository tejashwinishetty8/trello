import React from 'react';
import { getData } from '../Components/Data/getDataFromTrello';

class BoardContainer extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            boardData: '',
        }
    }
    componentDidMount() {
        getData().then(resp => resp.json())
        .then(data => { 
            this.setState({
                boardData: data
            })
        })
    }
    render(){
        const {name} = this.state.boardData;
        return(
            <div style = {{lineHeight:"100px"}}>
                <h1>{name}</h1>
            </div>
        );
    }
}

export default BoardContainer;